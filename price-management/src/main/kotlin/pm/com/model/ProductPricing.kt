package pm.com.model

import org.bson.codecs.pojo.annotations.BsonId
import org.bson.types.ObjectId

data class ProductPricing(
    @BsonId var pdtId:String? = ObjectId().toString(),
    var pdtManCost:Double,
    var pdtSaleCost:Double,
    var pdtDiscount:Double,
    var pdtDiscountSaleCost:Double,
    var isPdtEnable:Boolean,
    var lastModifiedDate:Long
    )