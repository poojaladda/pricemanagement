package pm.com.service

import pm.com.databasefactory.DatabaseFactory
import pm.com.model.ProductPricing
import pm.com.repository.ProductPricingRepo

class ProductPricingService:ProductPricingRepo {

    private val databaseFactory  = DatabaseFactory()

    override suspend fun generateProduct(): List<ProductPricing>? {
       return databaseFactory.generateProduct()
    }

    override suspend fun getAllProduct(): List<ProductPricing>? {
       return databaseFactory.getAllProduct()
    }


}