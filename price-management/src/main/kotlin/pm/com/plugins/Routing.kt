package pm.com.plugins

import io.ktor.routing.*
import io.ktor.application.*
import io.ktor.http.*
import io.ktor.response.*
import pm.com.dto.ResponseDto
import pm.com.model.ProductPricing
import pm.com.service.ProductPricingService
import java.util.*

fun Application.configureRouting() {

    val pdtService = ProductPricingService()
    routing {
        route("/api/pm/product"){

            get("/ping") {
                call.respondText("Welcome to the price management project!")
            }

            get("/generate") {

              val result : List<ProductPricing>? = pdtService.generateProduct()
                if(result==null)
                   return@get call.respond(ResponseDto(200,"Something Went Wrong",null))
                else
                    return@get call.respond(ResponseDto(200,"success",result))

            }

            get("/getAll") {
                val result : List<ProductPricing>? = pdtService.getAllProduct() as List<ProductPricing>?
                if(result==null)
                    return@get call.respond(ResponseDto(200,"Unable To Fetch Data",null))
                else
                    return@get call.respond(ResponseDto(200,"success",result))

            }


        }

    }
}
