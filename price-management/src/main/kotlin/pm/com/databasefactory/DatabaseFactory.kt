package pm.com.databasefactory

import org.litote.kmongo.coroutine.CoroutineCollection
import org.litote.kmongo.coroutine.coroutine
import org.litote.kmongo.eq
import org.litote.kmongo.reactivestreams.KMongo
import pm.com.model.ProductPricing
import java.util.*

class DatabaseFactory{

    private val client = KMongo.createClient().coroutine
    private val database = client.getDatabase("product_price_mgmt_db")
    val pdtCollection:CoroutineCollection<ProductPricing> = database.getCollection()



        /*  val pojoCodecRegistry: CodecRegistry = fromProviders(PojoCodecProvider.builder().automatic(true).build())
          val codecRegistry: CodecRegistry = fromRegistries(
              MongoClientSettings.getDefaultCodecRegistry(),
              pojoCodecRegistry
          )

          val clientSettings = MongoClientSettings.builder()
              .codecRegistry(codecRegistry)
              .build()

          val mongoClient = MongoClients.create(clientSettings)
          database = mongoClient.getDatabase("product_price_mgmt_db");
          userCollection = database.getCollection(ProductPricing::class.java.name, ProductPricing::class.java)
  */




    suspend fun generateProduct(): List<ProductPricing>? {
        val docList: List<ProductPricing> = arrayListOf(
            ProductPricing("Croma_"+UUID.randomUUID(), 200.0, 260.0, 0.0, 260.0, isPdtEnable = true, Date().time),
            ProductPricing("Croma_"+UUID.randomUUID(), 100.0, 180.0, 0.0, 180.0, isPdtEnable = true, Date().time),
            ProductPricing("Croma_"+UUID.randomUUID(), 400.0, 670.0, 0.0, 670.0, isPdtEnable = true, Date().time),
            ProductPricing("Croma_"+UUID.randomUUID(), 500.0, 980.0, 0.0, 980.0, isPdtEnable = true, Date().time),
            ProductPricing("Croma_"+UUID.randomUUID(), 800.0, 1260.0, 0.0, 1260.0, isPdtEnable = true, Date().time)
        );
        try{
            pdtCollection.insertMany(docList)
            return docList;
        }catch (ex:Exception){
            ex.printStackTrace()
        }
        return null;
    }

    suspend fun getAllProduct():List<ProductPricing>?{
            return pdtCollection.find().toList()
        }

    suspend fun getProductById(pdtId:String):ProductPricing?{
        return pdtCollection.findOne(ProductPricing::pdtId eq pdtId)

    }
}