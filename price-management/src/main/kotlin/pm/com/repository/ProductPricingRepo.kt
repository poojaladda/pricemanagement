package pm.com.repository

import pm.com.databasefactory.DatabaseFactory
import pm.com.model.ProductPricing

interface ProductPricingRepo {

    suspend fun generateProduct():List<ProductPricing>?

    suspend fun getAllProduct():List<ProductPricing>?

}